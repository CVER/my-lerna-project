(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('common-login', ['exports', '@angular/core'], factory) :
    (factory((global['common-login'] = {}),global.ng.core));
}(this, (function (exports,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CommonLoginService = (function () {
        function CommonLoginService() {
        }
        CommonLoginService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        CommonLoginService.ctorParameters = function () { return []; };
        /** @nocollapse */ CommonLoginService.ngInjectableDef = i0.defineInjectable({ factory: function CommonLoginService_Factory() { return new CommonLoginService(); }, token: CommonLoginService, providedIn: "root" });
        return CommonLoginService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CommonLoginComponent = (function () {
        function CommonLoginComponent() {
        }
        /**
         * @return {?}
         */
        CommonLoginComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        CommonLoginComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'common-login',
                        template: "\n    <p>\n      common-login works! Gilbertttt 2\n    </p>\n  ",
                        styles: []
                    },] },
        ];
        /** @nocollapse */
        CommonLoginComponent.ctorParameters = function () { return []; };
        return CommonLoginComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CommonLoginModule = (function () {
        function CommonLoginModule() {
        }
        CommonLoginModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [],
                        declarations: [CommonLoginComponent],
                        exports: [CommonLoginComponent]
                    },] },
        ];
        return CommonLoginModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.CommonLoginService = CommonLoginService;
    exports.CommonLoginComponent = CommonLoginComponent;
    exports.CommonLoginModule = CommonLoginModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLWxvZ2luLnVtZC5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vY29tbW9uLWxvZ2luL2xpYi9jb21tb24tbG9naW4uc2VydmljZS50cyIsIm5nOi8vY29tbW9uLWxvZ2luL2xpYi9jb21tb24tbG9naW4uY29tcG9uZW50LnRzIiwibmc6Ly9jb21tb24tbG9naW4vbGliL2NvbW1vbi1sb2dpbi5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb21tb25Mb2dpblNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjb21tb24tbG9naW4nLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxwPlxuICAgICAgY29tbW9uLWxvZ2luIHdvcmtzISBHaWxiZXJ0dHR0IDJcbiAgICA8L3A+XG4gIGAsXG4gIHN0eWxlczogW11cbn0pXG5leHBvcnQgY2xhc3MgQ29tbW9uTG9naW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbkxvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi9jb21tb24tbG9naW4uY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtDb21tb25Mb2dpbkNvbXBvbmVudF0sXG4gIGV4cG9ydHM6IFtDb21tb25Mb2dpbkNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgQ29tbW9uTG9naW5Nb2R1bGUgeyB9XG4iXSwibmFtZXMiOlsiSW5qZWN0YWJsZSIsIkNvbXBvbmVudCIsIk5nTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7UUFPRTtTQUFpQjs7b0JBTGxCQSxhQUFVLFNBQUM7d0JBQ1YsVUFBVSxFQUFFLE1BQU07cUJBQ25COzs7OztpQ0FKRDs7Ozs7OztBQ0FBO1FBYUU7U0FBaUI7Ozs7UUFFakIsdUNBQVE7OztZQUFSO2FBQ0M7O29CQWRGQyxZQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGNBQWM7d0JBQ3hCLFFBQVEsRUFBRSxpRUFJVDt3QkFDRCxNQUFNLEVBQUUsRUFBRTtxQkFDWDs7OzttQ0FWRDs7Ozs7OztBQ0FBOzs7O29CQUdDQyxXQUFRLFNBQUM7d0JBQ1IsT0FBTyxFQUFFLEVBQ1I7d0JBQ0QsWUFBWSxFQUFFLENBQUMsb0JBQW9CLENBQUM7d0JBQ3BDLE9BQU8sRUFBRSxDQUFDLG9CQUFvQixDQUFDO3FCQUNoQzs7Z0NBUkQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=