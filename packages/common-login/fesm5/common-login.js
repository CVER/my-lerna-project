import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CommonLoginService = /** @class */ (function () {
    function CommonLoginService() {
    }
    CommonLoginService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    CommonLoginService.ctorParameters = function () { return []; };
    /** @nocollapse */ CommonLoginService.ngInjectableDef = defineInjectable({ factory: function CommonLoginService_Factory() { return new CommonLoginService(); }, token: CommonLoginService, providedIn: "root" });
    return CommonLoginService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CommonLoginComponent = /** @class */ (function () {
    function CommonLoginComponent() {
    }
    /**
     * @return {?}
     */
    CommonLoginComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    CommonLoginComponent.decorators = [
        { type: Component, args: [{
                    selector: 'common-login',
                    template: "\n    <p>\n      common-login works! Gilbertttt 2\n    </p>\n  ",
                    styles: []
                },] },
    ];
    /** @nocollapse */
    CommonLoginComponent.ctorParameters = function () { return []; };
    return CommonLoginComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CommonLoginModule = /** @class */ (function () {
    function CommonLoginModule() {
    }
    CommonLoginModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [CommonLoginComponent],
                    exports: [CommonLoginComponent]
                },] },
    ];
    return CommonLoginModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { CommonLoginService, CommonLoginComponent, CommonLoginModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLWxvZ2luLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9jb21tb24tbG9naW4vbGliL2NvbW1vbi1sb2dpbi5zZXJ2aWNlLnRzIiwibmc6Ly9jb21tb24tbG9naW4vbGliL2NvbW1vbi1sb2dpbi5jb21wb25lbnQudHMiLCJuZzovL2NvbW1vbi1sb2dpbi9saWIvY29tbW9uLWxvZ2luLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIENvbW1vbkxvZ2luU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NvbW1vbi1sb2dpbicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBjb21tb24tbG9naW4gd29ya3MhIEdpbGJlcnR0dHQgMlxuICAgIDwvcD5cbiAgYCxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBDb21tb25Mb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTG9naW5Db21wb25lbnQgfSBmcm9tICcuL2NvbW1vbi1sb2dpbi5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW0NvbW1vbkxvZ2luQ29tcG9uZW50XSxcbiAgZXhwb3J0czogW0NvbW1vbkxvZ2luQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBDb21tb25Mb2dpbk1vZHVsZSB7IH1cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtJQU9FO0tBQWlCOztnQkFMbEIsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7Ozs7NkJBSkQ7Ozs7Ozs7QUNBQTtJQWFFO0tBQWlCOzs7O0lBRWpCLHVDQUFROzs7SUFBUjtLQUNDOztnQkFkRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGNBQWM7b0JBQ3hCLFFBQVEsRUFBRSxpRUFJVDtvQkFDRCxNQUFNLEVBQUUsRUFBRTtpQkFDWDs7OzsrQkFWRDs7Ozs7OztBQ0FBOzs7O2dCQUdDLFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUUsRUFDUjtvQkFDRCxZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztvQkFDcEMsT0FBTyxFQUFFLENBQUMsb0JBQW9CLENBQUM7aUJBQ2hDOzs0QkFSRDs7Ozs7Ozs7Ozs7Ozs7OyJ9