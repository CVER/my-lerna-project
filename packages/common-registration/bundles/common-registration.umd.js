(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('common-registration', ['exports', '@angular/core'], factory) :
    (factory((global['common-registration'] = {}),global.ng.core));
}(this, (function (exports,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CommonRegistrationService = (function () {
        function CommonRegistrationService() {
        }
        CommonRegistrationService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        CommonRegistrationService.ctorParameters = function () { return []; };
        /** @nocollapse */ CommonRegistrationService.ngInjectableDef = i0.defineInjectable({ factory: function CommonRegistrationService_Factory() { return new CommonRegistrationService(); }, token: CommonRegistrationService, providedIn: "root" });
        return CommonRegistrationService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CommonRegistrationComponent = (function () {
        function CommonRegistrationComponent() {
        }
        /**
         * @return {?}
         */
        CommonRegistrationComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        CommonRegistrationComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'lib-common-registration',
                        template: "\n    <p>\n      common-registration works! Alatttt 2\n    </p>\n  ",
                        styles: []
                    },] },
        ];
        /** @nocollapse */
        CommonRegistrationComponent.ctorParameters = function () { return []; };
        return CommonRegistrationComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CommonRegistrationModule = (function () {
        function CommonRegistrationModule() {
        }
        CommonRegistrationModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [],
                        declarations: [CommonRegistrationComponent],
                        exports: [CommonRegistrationComponent]
                    },] },
        ];
        return CommonRegistrationModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.CommonRegistrationService = CommonRegistrationService;
    exports.CommonRegistrationComponent = CommonRegistrationComponent;
    exports.CommonRegistrationModule = CommonRegistrationModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLXJlZ2lzdHJhdGlvbi51bWQuanMubWFwIiwic291cmNlcyI6WyJuZzovL2NvbW1vbi1yZWdpc3RyYXRpb24vbGliL2NvbW1vbi1yZWdpc3RyYXRpb24uc2VydmljZS50cyIsIm5nOi8vY29tbW9uLXJlZ2lzdHJhdGlvbi9saWIvY29tbW9uLXJlZ2lzdHJhdGlvbi5jb21wb25lbnQudHMiLCJuZzovL2NvbW1vbi1yZWdpc3RyYXRpb24vbGliL2NvbW1vbi1yZWdpc3RyYXRpb24ubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQ29tbW9uUmVnaXN0cmF0aW9uU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1jb21tb24tcmVnaXN0cmF0aW9uJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8cD5cbiAgICAgIGNvbW1vbi1yZWdpc3RyYXRpb24gd29ya3MhIEFsYXR0dHQgMlxuICAgIDwvcD5cbiAgYCxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBDb21tb25SZWdpc3RyYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vblJlZ2lzdHJhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vY29tbW9uLXJlZ2lzdHJhdGlvbi5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW0NvbW1vblJlZ2lzdHJhdGlvbkNvbXBvbmVudF0sXG4gIGV4cG9ydHM6IFtDb21tb25SZWdpc3RyYXRpb25Db21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIENvbW1vblJlZ2lzdHJhdGlvbk1vZHVsZSB7IH1cbiJdLCJuYW1lcyI6WyJJbmplY3RhYmxlIiwiQ29tcG9uZW50IiwiTmdNb2R1bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtRQU9FO1NBQWlCOztvQkFMbEJBLGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7O3dDQUpEOzs7Ozs7O0FDQUE7UUFhRTtTQUFpQjs7OztRQUVqQiw4Q0FBUTs7O1lBQVI7YUFDQzs7b0JBZEZDLFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUseUJBQXlCO3dCQUNuQyxRQUFRLEVBQUUscUVBSVQ7d0JBQ0QsTUFBTSxFQUFFLEVBQUU7cUJBQ1g7Ozs7MENBVkQ7Ozs7Ozs7QUNBQTs7OztvQkFHQ0MsV0FBUSxTQUFDO3dCQUNSLE9BQU8sRUFBRSxFQUNSO3dCQUNELFlBQVksRUFBRSxDQUFDLDJCQUEyQixDQUFDO3dCQUMzQyxPQUFPLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztxQkFDdkM7O3VDQVJEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9