import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CommonRegistrationService {
    constructor() { }
}
CommonRegistrationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
CommonRegistrationService.ctorParameters = () => [];
/** @nocollapse */ CommonRegistrationService.ngInjectableDef = defineInjectable({ factory: function CommonRegistrationService_Factory() { return new CommonRegistrationService(); }, token: CommonRegistrationService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CommonRegistrationComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
CommonRegistrationComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-common-registration',
                template: `
    <p>
      common-registration works! Alatttt 2
    </p>
  `,
                styles: []
            },] },
];
/** @nocollapse */
CommonRegistrationComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CommonRegistrationModule {
}
CommonRegistrationModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [CommonRegistrationComponent],
                exports: [CommonRegistrationComponent]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { CommonRegistrationService, CommonRegistrationComponent, CommonRegistrationModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLXJlZ2lzdHJhdGlvbi5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vY29tbW9uLXJlZ2lzdHJhdGlvbi9saWIvY29tbW9uLXJlZ2lzdHJhdGlvbi5zZXJ2aWNlLnRzIiwibmc6Ly9jb21tb24tcmVnaXN0cmF0aW9uL2xpYi9jb21tb24tcmVnaXN0cmF0aW9uLmNvbXBvbmVudC50cyIsIm5nOi8vY29tbW9uLXJlZ2lzdHJhdGlvbi9saWIvY29tbW9uLXJlZ2lzdHJhdGlvbi5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb21tb25SZWdpc3RyYXRpb25TZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLWNvbW1vbi1yZWdpc3RyYXRpb24nLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxwPlxuICAgICAgY29tbW9uLXJlZ2lzdHJhdGlvbiB3b3JrcyEgQWxhdHR0dCAyXG4gICAgPC9wPlxuICBgLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIENvbW1vblJlZ2lzdHJhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uUmVnaXN0cmF0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9jb21tb24tcmVnaXN0cmF0aW9uLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbQ29tbW9uUmVnaXN0cmF0aW9uQ29tcG9uZW50XSxcbiAgZXhwb3J0czogW0NvbW1vblJlZ2lzdHJhdGlvbkNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgQ29tbW9uUmVnaXN0cmF0aW9uTW9kdWxlIHsgfVxuIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0lBT0UsaUJBQWlCOzs7WUFMbEIsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7Ozs7Ozs7O0FDSkQ7SUFhRSxpQkFBaUI7Ozs7SUFFakIsUUFBUTtLQUNQOzs7WUFkRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtnQkFDbkMsUUFBUSxFQUFFOzs7O0dBSVQ7Z0JBQ0QsTUFBTSxFQUFFLEVBQUU7YUFDWDs7Ozs7Ozs7O0FDVkQ7OztZQUdDLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUUsRUFDUjtnQkFDRCxZQUFZLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztnQkFDM0MsT0FBTyxFQUFFLENBQUMsMkJBQTJCLENBQUM7YUFDdkM7Ozs7Ozs7Ozs7Ozs7OzsifQ==