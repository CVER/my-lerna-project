import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CommonRegistrationService = /** @class */ (function () {
    function CommonRegistrationService() {
    }
    CommonRegistrationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    CommonRegistrationService.ctorParameters = function () { return []; };
    /** @nocollapse */ CommonRegistrationService.ngInjectableDef = defineInjectable({ factory: function CommonRegistrationService_Factory() { return new CommonRegistrationService(); }, token: CommonRegistrationService, providedIn: "root" });
    return CommonRegistrationService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CommonRegistrationComponent = /** @class */ (function () {
    function CommonRegistrationComponent() {
    }
    /**
     * @return {?}
     */
    CommonRegistrationComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    CommonRegistrationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-common-registration',
                    template: "\n    <p>\n      common-registration works! Alatttt 2\n    </p>\n  ",
                    styles: []
                },] },
    ];
    /** @nocollapse */
    CommonRegistrationComponent.ctorParameters = function () { return []; };
    return CommonRegistrationComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CommonRegistrationModule = /** @class */ (function () {
    function CommonRegistrationModule() {
    }
    CommonRegistrationModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [CommonRegistrationComponent],
                    exports: [CommonRegistrationComponent]
                },] },
    ];
    return CommonRegistrationModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { CommonRegistrationService, CommonRegistrationComponent, CommonRegistrationModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLXJlZ2lzdHJhdGlvbi5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vY29tbW9uLXJlZ2lzdHJhdGlvbi9saWIvY29tbW9uLXJlZ2lzdHJhdGlvbi5zZXJ2aWNlLnRzIiwibmc6Ly9jb21tb24tcmVnaXN0cmF0aW9uL2xpYi9jb21tb24tcmVnaXN0cmF0aW9uLmNvbXBvbmVudC50cyIsIm5nOi8vY29tbW9uLXJlZ2lzdHJhdGlvbi9saWIvY29tbW9uLXJlZ2lzdHJhdGlvbi5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb21tb25SZWdpc3RyYXRpb25TZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLWNvbW1vbi1yZWdpc3RyYXRpb24nLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxwPlxuICAgICAgY29tbW9uLXJlZ2lzdHJhdGlvbiB3b3JrcyEgQWxhdHR0dCAyXG4gICAgPC9wPlxuICBgLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIENvbW1vblJlZ2lzdHJhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uUmVnaXN0cmF0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9jb21tb24tcmVnaXN0cmF0aW9uLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbQ29tbW9uUmVnaXN0cmF0aW9uQ29tcG9uZW50XSxcbiAgZXhwb3J0czogW0NvbW1vblJlZ2lzdHJhdGlvbkNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgQ29tbW9uUmVnaXN0cmF0aW9uTW9kdWxlIHsgfVxuIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0lBT0U7S0FBaUI7O2dCQUxsQixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7OztvQ0FKRDs7Ozs7OztBQ0FBO0lBYUU7S0FBaUI7Ozs7SUFFakIsOENBQVE7OztJQUFSO0tBQ0M7O2dCQWRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUseUJBQXlCO29CQUNuQyxRQUFRLEVBQUUscUVBSVQ7b0JBQ0QsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7Ozs7c0NBVkQ7Ozs7Ozs7QUNBQTs7OztnQkFHQyxRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFLEVBQ1I7b0JBQ0QsWUFBWSxFQUFFLENBQUMsMkJBQTJCLENBQUM7b0JBQzNDLE9BQU8sRUFBRSxDQUFDLDJCQUEyQixDQUFDO2lCQUN2Qzs7bUNBUkQ7Ozs7Ozs7Ozs7Ozs7OzsifQ==