import { TestBed, inject } from '@angular/core/testing';

import { CommonRegistrationService } from './common-registration.service';

describe('CommonRegistrationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommonRegistrationService]
    });
  });

  it('should be created', inject([CommonRegistrationService], (service: CommonRegistrationService) => {
    expect(service).toBeTruthy();
  }));
});
