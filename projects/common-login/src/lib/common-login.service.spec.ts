import { TestBed, inject } from '@angular/core/testing';

import { CommonLoginService } from './common-login.service';

describe('CommonLoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommonLoginService]
    });
  });

  it('should be created', inject([CommonLoginService], (service: CommonLoginService) => {
    expect(service).toBeTruthy();
  }));
});
